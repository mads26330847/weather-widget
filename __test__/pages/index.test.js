import { render, fireEvent, cleanup, waitFor, screen } from '@testing-library/react';
import React from 'react';
import * as nextRouter from 'next/router';
import { enableFetchMocks } from 'jest-fetch-mock';

import Index from '../../pages/index';
import { getWeather } from '../../services/api';

enableFetchMocks();

nextRouter.useRouter = jest.fn();
const replace = jest.fn();
nextRouter.useRouter.mockImplementation(() => ({ route: '/', replace }));

beforeEach(() => {
    jest.clearAllMocks();
});
afterEach(cleanup);

const aalborgWeather = { tempCelcius: 21, humidityPercentage: 70, windMS: 5.4, windDegrees: 209 };

jest.mock('../../services/api', () => {
    return {
        getWeather: jest.fn().mockImplementation((city) => {
            if (city == 'aalborg') {
                return { city, ...aalborgWeather };
            } else {
                return {
                    city,
                    tempCelcius: 20,
                    humidityPercentage: 90,
                    windMS: 8.4,
                    windDegrees: 89
                };
            }
        })
    };
});

describe('It renders and matches snapshot', () => {
    it('renders', async () => {
        const props = await Index.getInitialProps({ query: {} }); //No query params
        const { asFragment } = render(<Index {...props} />);
        expect(asFragment()).toMatchSnapshot();
        expect(getWeather).toHaveBeenCalledWith('copenhagen');
        expect(replace).toHaveBeenCalledTimes(1);
        expect(replace).toHaveBeenCalledWith(expect.stringMatching(/city=copenhagen/), undefined, {
            shallow: true
        });
    });
});

describe('It renders and matches snapshot with query city=odense', () => {
    it('renders', async () => {
        const city = 'odense';
        const props = await Index.getInitialProps({ query: { city } });
        const { asFragment } = render(<Index {...props} />);
        expect(asFragment()).toMatchSnapshot();
        expect(getWeather).toHaveBeenCalledWith(city);
        expect(replace).toHaveBeenCalledTimes(1);
        expect(replace).toHaveBeenCalledWith(expect.stringMatching(/city=odense/), undefined, {
            shallow: true
        });
    });
});

describe('It reloads data on search and shows loading message', () => {
    it('renders', async () => {
        fetch.mockResponseOnce(JSON.stringify({ city: 'aalborg', ...aalborgWeather }));

        const props = await Index.getInitialProps({ query: {} }); //No query params
        const { asFragment } = render(<Index {...props} />);
        expect(getWeather).toHaveBeenCalledWith('copenhagen');
        expect(replace).toHaveBeenCalledTimes(1);
        const input = screen.getByPlaceholderText('City');
        fireEvent.change(input, { target: { value: 'aalborg' } });
        expect(input.value).toBe('aalborg');
        const button = screen.getByText('Search');
        fireEvent.click(button);
        await waitFor(() => {
            expect(screen.getByText('...Loading')).toBeInTheDocument();
        });
        await waitFor(() => {
            expect(screen.queryByText('...Loading')).not.toBeInTheDocument();
        });
        expect(fetch).toBeCalledTimes(1);
        expect(screen.getByTestId('city')).toHaveTextContent('aalborg');
        expect(screen.getByTestId('humidity')).toHaveTextContent(aalborgWeather.humidityPercentage);
        expect(screen.getByTestId('wind')).toHaveTextContent(`${aalborgWeather.windMS} m/s S`);
        expect(replace).toHaveBeenCalledWith(expect.stringMatching(/city=aalborg/), undefined, {
            shallow: true
        });
        expect(replace).toHaveBeenCalledTimes(2);
        expect(asFragment()).toMatchSnapshot();
    });
});
