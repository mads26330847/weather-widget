import { enableFetchMocks } from 'jest-fetch-mock';

import { getWeather } from '../../../services/api';

enableFetchMocks();

const windObject = {
    main: { temp: 10, humidity: 60 },
    wind: { speed: 10, deg: 90 },
    name: 'odense',
    cod: 200
};

describe('testing api', () => {
    beforeEach(() => {
        fetch.resetMocks();
    });

    it('calls weathermap api and returns response', () => {
        fetch.mockResponseOnce(JSON.stringify(windObject));

        getWeather('odense').then((res) => {
            expect(res.city).toEqual('odense');
            expect(res.windDegrees).toBe(windObject.wind.deg);
            expect(res.windMS).toBe(windObject.wind.speed);
            expect(res.tempCelcius).toBe(windObject.main.temp);
            expect(res.humidityPercentage).toBe(windObject.main.humidity);
        });

        //assert on the times called and arguments given to fetch
        expect(fetch.mock.calls.length).toEqual(1);
    });

    it('calls weathermap api and dont get a response', async () => {
        fetch.mockReject(new Error('Internal error'));

        try {
            await getWeather('odense');
            expect(true).toBe(false); //fail if we reach here
        } catch (err) {
            expect(err.message).toBe('Failed to fetch weather');
        }

        //assert on the times called and arguments given to fetch
        expect(fetch.mock.calls.length).toEqual(1);
    });
});
