import { render, cleanup, screen } from '@testing-library/react';
import React from 'react';

import Widget from '../../components/Widget';

afterEach(cleanup);

const skagenWeather = {
    city: 'Skagen',
    tempCelcius: 31,
    humidityPercentage: 20,
    windMS: 1.4,
    windDegrees: 301
};

describe('It renders with weatherdata', () => {
    it('renders', async () => {
        const { asFragment } = render(
            <Widget
                loading={false}
                humidityPercentage={skagenWeather.humidityPercentage}
                tempCelcius={skagenWeather.tempCelcius}
                windMS={skagenWeather.windMS}
                windDegrees={skagenWeather.windMS}
                city={skagenWeather.city}
            />
        );
        expect(screen.queryByText('...Loading')).not.toBeInTheDocument();
        expect(screen.getByTestId('city')).toHaveTextContent(skagenWeather.city);
        expect(screen.getByTestId('humidity')).toHaveTextContent(skagenWeather.humidityPercentage);
        expect(screen.getByTestId('wind')).toHaveTextContent(`${skagenWeather.windMS} m/s N`);
        //should also check rest of parameters...
        expect(asFragment()).toMatchSnapshot();
    });
});

describe('It renders loading message', () => {
    it('renders', async () => {
        const { asFragment } = render(<Widget loading={true} />);
        expect(screen.getByText('...Loading')).toBeInTheDocument();
        expect(asFragment()).toMatchSnapshot();
    });
});

describe('It renders error message', () => {
    it('renders', async () => {
        const { asFragment } = render(<Widget loading={false} errorMessage={'This is an error'} />);
        expect(screen.getByText('This is an error')).toBeInTheDocument();
        expect(asFragment()).toMatchSnapshot();
    });
});
