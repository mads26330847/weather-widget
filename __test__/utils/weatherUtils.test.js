import { getDirectionFromDegrees } from '../../utils/weatherUtils';

describe('Degree to direction', () => {
    test('Should return north when degrees is 0', () => {
        expect(getDirectionFromDegrees(0)).toBe('N');
    });
    test('Should return north when degrees is 44', () => {
        expect(getDirectionFromDegrees(44)).toBe('N');
    });
    test('Should return east when degrees is 45', () => {
        expect(getDirectionFromDegrees(45)).toBe('E');
    });
    test('Should return east when degrees is 134', () => {
        expect(getDirectionFromDegrees(134)).toBe('E');
    });
    test('Should return south when degrees is 135', () => {
        expect(getDirectionFromDegrees(135)).toBe('S');
    });
    test('Should return south when degrees is 224', () => {
        expect(getDirectionFromDegrees(224)).toBe('S');
    });
    test('Should return west when degrees is 225', () => {
        expect(getDirectionFromDegrees(225)).toBe('W');
    });
    test('Should return west when degrees is 314', () => {
        expect(getDirectionFromDegrees(314)).toBe('W');
    });

    test('Should return North when degrees is 315', () => {
        expect(getDirectionFromDegrees(315)).toBe('N');
    });

    test('Should return west when degrees is -1', () => {
        expect(getDirectionFromDegrees(-1)).toBe('N');
    });
});
