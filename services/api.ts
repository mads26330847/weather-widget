import { Weather } from '../interfaces';

const appID = process.env.WEATHER_APP_ID;

interface OpenWeatherResponse {
    main?: {
        temp: number;
        humidity: number;
    };
    wind?: {
        speed: number;
        deg: number;
    };
    name?: string;
    cod: number;
    message?: string;
}

export const getWeather = async (
    city: string,
    country: string = 'dk'
): Promise<Weather | Error> => {
    try {
        const weatherResponse = await fetch(
            `http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&units=metric&appid=${appID}`
        );
        const weatherData: OpenWeatherResponse = await weatherResponse.json();
        return {
            city: weatherData.name,
            tempCelcius: weatherData.main?.temp,
            humidityPercentage: weatherData.main?.humidity,
            windMS: weatherData.wind?.speed,
            windDegrees: weatherData.wind?.deg,
            errorMessage: weatherData.message
        };
    } catch (err) {
        throw new Error('Failed to fetch weather');
    }
};

export default { getWeather };
