export interface Weather {
    city?: string;
    tempCelcius?: number;
    humidityPercentage?: number;
    windMS?: number;
    windDegrees?: number;
    errorMessage?: string;
}
