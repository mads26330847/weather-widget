import type { NextApiRequest, NextApiResponse } from 'next';
import { getWeather } from '../../services/api';

type WeatherRequest = NextApiRequest & {
    query: { city?: string };
};

export default async function handler(req: WeatherRequest, res: NextApiResponse) {
    if (!req.query.city) {
        return res.status(400).json({ errorMessage: 'Missing city' });
    }
    const { city } = req.query;
    try {
        const weatherData = await getWeather(city);
        res.status(200).json({ ...weatherData });
    } catch (err) {
        res.status(500).json({ errorMessage: 'Internal server error' });
    }
}
