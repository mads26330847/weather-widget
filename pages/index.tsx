import React, { useState, useEffect } from 'react';
import { NextPageContext } from 'next';
import Widget from '../components/Widget';
import { getWeather } from '../services/api';
import { Weather } from '../interfaces';
import { useRouter } from 'next/router';

export default function Index(props: Weather): JSX.Element {
    const router = useRouter();
    const [city, setCity] = useState<string>(props.city || '');
    const [weather, setWeather] = useState<Weather>(props);
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {
        router.replace(`${router.pathname}?city=${city}`, undefined, { shallow: true }); //when city changes, also change the url
    }, [city]);

    const searchCity = async (city: string) => {
        setLoading(true);
        setCity(city);
        try {
            const weatherDataResponse = await fetch(`./api/weather?city=${city}`);
            const weatherData = await weatherDataResponse.json();
            setWeather(weatherData as Weather);
        } catch (err) {
            console.log(err);
        } finally {
            setLoading(false);
        }
    };

    return (
        <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Widget
                loading={loading}
                city={weather.city}
                tempCelcius={weather.tempCelcius}
                humidityPercentage={weather.humidityPercentage}
                windMS={weather.windMS}
                windDegrees={weather.windDegrees}
                searchCity={searchCity}
                errorMessage={weather.errorMessage}
            />
        </div>
    );
}

Index.getInitialProps = async (ctx: NextPageContext) => {
    let city: string = 'copenhagen'; // by default use copenhagen
    if (ctx.query.city) {
        //if query param city exist, use the query param instead
        city = ctx.query.city as string;
    }
    const json = await getWeather(city);
    return json;
};
