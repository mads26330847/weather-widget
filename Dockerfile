FROM node:15.7.0

WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install --pure-lockfile


COPY . .
RUN yarn build


EXPOSE 3000

ENV WEATHER_APP_ID="API KEY HERE"
CMD [ "yarn", "start" ]
