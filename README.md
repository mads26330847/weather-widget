# Weather Widget

Weather widget is an application written in next.js with react to show weather information for danish cities

## Installation and usage

To test application using docker set WEATHER_APP_ID in the Dockerfile to a valid openweathermap app id
Build the image using:

```bash
docker build -t weatherapp .
```

Run the app using:

```bash
 docker run -p 3000:3000 weatherapp
```

To run locally:
Set environment variable WEATHER_APP_ID to a valid openweathermap app id
run "yarn" to fetch the necessary packages

run the application using

```bash
yarn dev
```

To run the tests:

```bash
yarn test
```
