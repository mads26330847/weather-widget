import React, { useState } from 'react';
import { Weather } from '../interfaces';
import { getDirectionFromDegrees } from '../utils/weatherUtils';

type props = Weather & { loading: boolean; searchCity: (city: string) => void };

const Widget = (props: props): JSX.Element => {
    const [city, setCity] = useState<string>('');

    const onSearchCity = (e: React.FormEvent<HTMLFormElement>): void => {
        e.preventDefault();
        props.searchCity(city);
    };
    if (props.loading) {
        return <div>...Loading</div>;
    }

    return (
        <div className="widget" style={{ margin: '10px', width: '300px' }}>
            <div className={`panel panel-${props.errorMessage ? 'warning' : 'info'}`}>
                <div className="panel-heading">
                    {props.errorMessage ? (
                        props.errorMessage
                    ) : (
                        <>
                            Weather in <b data-testid="city">{props.city}</b>
                        </>
                    )}
                </div>
                <ul className="list-group">
                    <li className="list-group-item">
                        Temperature:
                        <b>
                            {props.tempCelcius} {props.tempCelcius ? '°C' : ''}
                        </b>
                    </li>
                    <li className="list-group-item">
                        Humidity: <b data-testid="humidity">{props.humidityPercentage}</b>
                    </li>
                    <li className="list-group-item">
                        Wind:
                        <b data-testid="wind">
                            {props.windMS && props.windDegrees
                                ? `${props.windMS} m/s ${getDirectionFromDegrees(
                                      props.windDegrees
                                  )}`
                                : ''}
                        </b>
                    </li>
                    <li className="list-group-item">
                        <form className="form-inline" action="/" onSubmit={onSearchCity}>
                            {/* if no js it will query self */}
                            <div className="form-group">
                                <input
                                    value={city}
                                    onChange={(e) => setCity(e.target.value)}
                                    type="text"
                                    name="city"
                                    className="form-control"
                                    id="city"
                                    placeholder="City"
                                />
                                {/* if no js the param city will be used */}
                            </div>
                            <button type="submit" className="btn btn-default">
                                Search
                            </button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default Widget;
