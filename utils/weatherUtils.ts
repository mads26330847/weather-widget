type Directions = 'N' | 'S' | 'E' | 'W';
const directions: Array<Directions> = ['N', 'E', 'S', 'W'];

export const getDirectionFromDegrees = (degrees: number): Directions => {
    const index =
        (Math.round((degrees * directions.length) / 360) + directions.length) % directions.length;
    return directions[index];
};
